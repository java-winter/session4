package nonabstractExample;

public class App {
	public static void main(String[] args) {
		GooglePixle1 p1 = new GooglePixle1();
		GooglePixle2 p2 = new GooglePixle2();
		GooglePixle3 p3 = new GooglePixle3();
	}
	
	//I would like to show these info to the users
	//I cannot make a general method to call the object
	
	public void print(GooglePixle1 p1) {
		//I had to repeat
		System.out.println(p1.getLifeType());
	}
	public void print(GooglePixle2 p2) {
		//I had to repeat
		System.out.println(p2.getLifeType());		
	}

	public void print(GooglePixle3 p3) {
		//I had to repeat
		System.out.println(p3.getLifeType());		
	}

}

