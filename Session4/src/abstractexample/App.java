package abstractexample;

public class App {

	public static void main(String[] args) {
		GooglePixel phone1 = new GooglePixel1();
		GooglePixel phone2 = new GooglePixel2();
		GooglePixel phone3 = new GooglePixel3();
		
		printOutput(phone1);
		printOutput(phone2);
		printOutput(phone3);

	}
	
	public static void printOutput(GooglePixel gPhone) {
		System.out.println(gPhone.getLifeType());
	}

}
