package wrapper;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		//primitive data types
		int x1 = 10; 
		Integer x2 = 20; // Wrapper Class
		
		//Generics accept Class and not primitive
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(25);
		
		//Accepting null values to a fields	
		int x3 = 0; //default value = 0
		Integer x4 = null;
		
		System.out.println(x3);
		System.out.println(x4);
		
		//I passed a wrapper instead of a primitive and
		//the methods automatically unbox the value
		checkValue1(x4);
		
		
		//I passed a primitive instead of wrapper 
		//and the method accepts and autobox the primtive
		checkValue2(x3);
		
		
		char ch1 = 'a';
		Character ch2 = 'b';
		
		checkCharacter(ch2);
		
		
		//For calling some methods we can use Wrapper classes
		int parseInt = Integer.parseInt("12");
		int compare = Integer.compare(12, 5);
		
	}
	
	private static void checkCharacter(Character x) {
		
	}

	private static void checkValue1(int num) {
		
	}
	
	private static void checkValue2(Integer num) {
	}
	
	

}
