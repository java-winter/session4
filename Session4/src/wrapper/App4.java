package wrapper;

class Animal{
	
	public void makeSound(String name) {
		System.out.println("sound of a " + name);
	}
}

class Cat extends Animal{
	
	@Override
	public void makeSound(String name) {
		System.out.println("meow");
	}
	
	public void makeSound(String name, int age) {
		//super.makeSound("cat"); this line is optional
		System.out.println("cat is calling");
	}
}

public class App4 {

	public static void main(String[] args) {
		//Overloading => means in the same class having different methods with the same 
		//name and different parameters type
		printSum(20.5, 10.5);
		printSum(20, 10);
		printSum("20", "10");
		
		
		// an example of overloading
//		StringBuilder b = new StringBuilder();
//		b.append(b)
	}
	
	public static void printSum(int a, int b) {
		System.out.println("sum of 2 integers are " +  (a + b));
	}

	public static void printSum(int a, double c) {
		System.out.println("sum of 2 integers are " +  (a));
	}

	
	public static void printSum(int a) {
		System.out.println("sum of 2 integers are " +  (a));
	}
	
	//this is not acceptable since I only changed the name of the parameter
//	public static void printSum(int b) {
//		System.out.println("sum of 2 integers are " +  (a + b));
//	}

	
	public static void printSum(double a, double b) {
		System.out.println("sum of 2 doubles are " +  (a + b));
	}
	
	public static void printSum(String a, String b) {
		System.out.println("sum of 2 strings are " +  (Integer.parseInt(a) + Integer.parseInt(b)));
	}

}
