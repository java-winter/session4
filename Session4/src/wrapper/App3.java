package wrapper;

class Machine{
	private String name;
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void sayMyName() {
		System.out.println("I am a Machine");
	}
	
	//default constructor
//	public Machine() {
//		
//	}
	
	public Machine(String name) {
		this.name = name;
	}
	
	public Machine(String name, int age) {
		
	}
	
}

//inheritance : left class is a child of right class
class Car extends Machine{
	
	int wheelCount;
	//defining the constructor 
	//to call the parent constructor
	public Car(String name, int wheelCount) {
		super(name);// super is used to call the parent constructor
		this.wheelCount = wheelCount;		
	}
		
	public void printName() {
		System.out.println(getName());
	}
	
	@Override // you are overriding a method in the parent class
	public void sayMyName() {
		super.sayMyName(); //super is used to call the parent class (similar to this keyword)
							//that used for calling the current class
		System.out.println("I am a car");
	}
}

class Truck extends Machine{
	
	double capacity;
	
	public Truck(String name, int capacity) {
		super(name);
		this.capacity = capacity;
	}	
	
	@Override 
	public void sayMyName() {
		System.out.println("I am a truck");
	}
}

public class App3 {

	//inheritance
	public static void main(String[] args) {
//		Machine machine = new Machine();
//		machine.setName("Robot");
		
		//inheritance => use of methods that is defined in the parent class
		Car car = new Car("Honda", 4);
//		car.setName("Honda");
		car.printName();
		
		car.sayMyName();
		
		Truck truck = new Truck("MYTruck", 5000);
		truck.sayMyName();
	}

}
